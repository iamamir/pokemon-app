# PokemonApp
### Angular application. The app consists of four different views, namely Login, Catalogue, Trainer and PokemonDetails. In order to use the app the user must login, once the user is logged, s/he will then be able to go through the Catalogue which contains a list of pokemon cards that are fetched from the PokemonAPI. The user can then click on a specific pokemon to see the details, which will direct the user to the PokemonDetails page. The PokemonDetails page, displays the details of a choosen pokemon as well as a button to collect the pokemon. The collect button adds that specific pokemon to the user's list of collected pokemons, that is displayed on the Trainer page. 

### ![screenshot](https://gitlab.com/iamamir/pokemon-app/-/raw/master/src/assets/images/login-design.PNG)
### ![screenshot](https://gitlab.com/iamamir/pokemon-app/-/raw/master/src/assets/images/home-design.PNG)
### ![screenshot](https://gitlab.com/iamamir/pokemon-app/-/raw/master/src/assets/images/pokemon-detail-design.PNG)
### ![screenshot](https://gitlab.com/iamamir/pokemon-app/-/raw/master/src/assets/images/profile-design.PNG)


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
